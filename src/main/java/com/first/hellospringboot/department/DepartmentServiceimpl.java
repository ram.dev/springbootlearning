package com.first.hellospringboot.department;

import com.first.hellospringboot.entity.Department;
import com.first.hellospringboot.exceptionHandler.DepartmentNotExistException;
import com.first.hellospringboot.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DepartmentServiceimpl implements DepartmentService {


    @Autowired
    private DepartmentRepository departmentRepository;

    /**
     * @param department
     * @return
     */
    @Override
    public Department addDepartment(Department department) {
        return departmentRepository.save(department);
    }

    @Override
    public List<Department> getAllDepartment() {

        List<Department> all = departmentRepository.findAll();
        Sort sort = Sort.by("departmentName");

        Pageable pageable = PageRequest.of(0, 2, Sort.by(Sort.Direction.DESC, "departmentName"));

        Page<Department> departmentPage = departmentRepository.findAll(pageable);
        System.out.println(departmentPage.getTotalElements());
        System.out.println(departmentPage.isFirst());
        System.out.println(departmentPage.isLast());
        return departmentPage.getContent();

    }

    @Override
    public Department updateDepartment(Long id, Department department) {

        Department updateDepartment = departmentRepository.findById(id)
                .get();

        if (department.getDepartmentName() != null) updateDepartment.setDepartmentName(department.getDepartmentName());
        if (department.getDepartmentCode() != null) updateDepartment.setDepartmentCode(department.getDepartmentCode());

        return departmentRepository.save(updateDepartment);
    }

    @Override
    public void deleteDepartment(Long id) {
        departmentRepository.deleteById(id);
    }

    @Override
    public Department getDepartmentById(Long id) throws DepartmentNotExistException {

        Optional<Department> department = departmentRepository.findById(id);

        if (department.isEmpty())
            throw new DepartmentNotExistException("Department with the '" + id + "' doesnt exist");

        return department.get();
    }


}
