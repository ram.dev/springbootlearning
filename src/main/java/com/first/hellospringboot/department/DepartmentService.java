package com.first.hellospringboot.department;

import com.first.hellospringboot.entity.Department;
import com.first.hellospringboot.exceptionHandler.DepartmentNotExistException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DepartmentService {
    Department addDepartment(Department department);

    List<Department> getAllDepartment();

    Department updateDepartment(Long id, Department department);

    void deleteDepartment(Long id);

    Department getDepartmentById(Long id) throws DepartmentNotExistException;
}


