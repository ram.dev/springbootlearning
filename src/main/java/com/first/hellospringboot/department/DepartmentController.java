package com.first.hellospringboot.department;

import com.first.hellospringboot.entity.Department;
import com.first.hellospringboot.exceptionHandler.DepartmentNotExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;


    @PostMapping("/addDepartment")
    public Department addDepartment(@RequestBody Department department) {
        return departmentService.addDepartment(department);
    }

    @GetMapping("/getAllDepartments")
    public List<Department> getAllDepartment() {
        return departmentService.getAllDepartment();
    }

    @PutMapping("/updateDepartment/{id}")
    public Department updateDepartment(@PathVariable("id") Long id, @RequestBody Department department) {
        return departmentService.updateDepartment(id, department);
    }

    @DeleteMapping("/deleteDepartment/{id}")
    public String deleteDepartment(@PathVariable("id") Long id) {
        departmentService.deleteDepartment(id);
        return "Department Deleted Successfully";
    }

    @GetMapping("/getDepartmentById/{id}")
    public Department getDepartmentById(@PathVariable("id") Long id) throws DepartmentNotExistException {
        return departmentService.getDepartmentById(id);
    }
}