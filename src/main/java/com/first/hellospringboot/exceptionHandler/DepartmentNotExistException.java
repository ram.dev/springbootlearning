package com.first.hellospringboot.exceptionHandler;

public class DepartmentNotExistException extends Exception {

    public DepartmentNotExistException() {
        super();
    }

    public DepartmentNotExistException(String message) {
        super(message);
    }

    public DepartmentNotExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public DepartmentNotExistException(Throwable cause) {
        super(cause);
    }

    protected DepartmentNotExistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
