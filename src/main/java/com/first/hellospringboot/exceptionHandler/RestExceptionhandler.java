package com.first.hellospringboot.exceptionHandler;

import com.first.hellospringboot.entity.ErrorMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class RestExceptionhandler extends ResponseEntityExceptionHandler {

    @Autowired
    private ErrorMessage errorMessage;

    @ExceptionHandler(DepartmentNotExistException.class)
    public ResponseEntity<ErrorMessage> departmentNotFoundException(DepartmentNotExistException departmentNotExistException){

        errorMessage.setMessage(departmentNotExistException.getMessage());
        errorMessage.setStatusCode(HttpStatus.NOT_FOUND.value());
        errorMessage.setHttpStatus(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(errorMessage,HttpStatus.NOT_FOUND);
    }
}
