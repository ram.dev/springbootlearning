package com.first.hellospringboot.entity;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
@Data
public class ErrorMessage {

    private Integer statusCode;
    private HttpStatus httpStatus;
    private String message;
}
