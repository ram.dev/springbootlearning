package com.first.hellospringboot.repository;

import com.first.hellospringboot.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<Department, Long> {

    /* List<Department> findDistinctByDepartmentNameAndDepartmentCodeOrderByIdDesc();*/
}
